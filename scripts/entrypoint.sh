#!/bin/sh

set -e

# collect static files
python manage.py collectstatic --noinput

# check if db up and run migrations
python manage.py wait_for_db
python manage.py migrate

# run the uwsgi service (tcp on port 9000, 4 workers per docker container)
uwsgi --socket :9000 --workers 4 --master --enable-threads --module app.wsgi