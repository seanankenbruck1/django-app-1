variable "prefix" {
  default = "ado1"
}

variable "project" {
  default = "django-app-1"
}

variable "contact" {
  default = "contact@ankenbruckdevops.com"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "django-app-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for api"

  default = "205743848741.dkr.ecr.us-east-1.amazonaws.com/django-app-1:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"

  default = "205743848741.dkr.ecr.us-east-1.amazonaws.com/django-app-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret for Django app"
}
